#!/bin/bash
mkdir -p /var/www/html/dp_new/bootstrap/cache
mkdir -p /var/www/html/dp_new/storage/framework
mkdir -p /var/www/html/dp_new/storage/framework/sessions
mkdir -p /var/www/html/dp_new/storage/framework/views
mkdir -p /var/www/html/dp_new/storage/framework/cache
composer install -d /var/www/html/dp_new/
sudo chown :www-data /var/www/html/dp_new/app -R
sudo chown :www-data /var/www/html/dp_new/storage -R
sudo chown :www-data /var/www/html/dp_new/bootstrap -R
sudo chmod 777 /var/www/html/dp_new/app -R
sudo chmod 777 /var/www/html/dp_new/storage -R
sudo chmod 777 /var/www/html/dp_new/bootstrap -R
